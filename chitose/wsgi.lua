--- -*- mode: Lua; lua-indent-level: 4; -*- ---

local mod = {}

function mod.run_wsgi (entry_point, wire_protocol)
    if wire_protocol == "cgi" then
        local runner = require("chitose._wsgi.from_cgi")
        return runner(entry_point)
    else
        error("unknown wire protocol")
    end  -- wire protocols
end  -- run_wsgi

return mod
