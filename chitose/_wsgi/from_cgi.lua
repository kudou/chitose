--- -*- mode: Lua; lua-indent-level: 4; -*- ---

local posix_stdlib = require "posix.stdlib"
local wsgi_base = require "chitose._wsgi.base"

local function from_cgi (callback)
    local env = posix_stdlib.getenv()
    local mock = wsgi_base.Mock(env, callback, io.stdin, io.stdout)
    return mock:bootstrap()
end

return from_cgi
