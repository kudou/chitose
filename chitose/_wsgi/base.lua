--- -*- mode: Lua; lua-indent-level: 4; -*- ---

local stringx = require "pl.stringx"

local HttpHeaders = require "chitose.HttpHeaders"
local base = {}
local wsgi = {}

local CGI_VARS = {
    "REQUEST_METHOD", "PATH_INFO", "QUERY_STRING", "CONTENT_LENGTH",
    "CONTENT_TYPE", "SCRIPT_NAME", "SERVER_NAME", "SERVER_PORT",
    "SERVER_PROTOCOL"
}

local OPTIONAL_VARS = {
    ["REMOTE_ADDR"] = "0.0.0.0",
    ["REMOTE_PORT"] = "0",
    ["REQUEST_URI"] = ""
}

function wsgi.Mock (env, entry_function, stdin, stdout)
    local result = {
        env = env,
        entry_function = entry_function,
        stdin = stdin,
        stdout = stdout
    }
    setmetatable(result, {__index = base})
    return result
end

function base:bootstrap ()
    -- Prepare the "environ" variable
    local all_envs = self.env
    local environ = {
        ["wsgi.input"] = self.stdin,
        ["wsgi.errors"] = self.stderr,
        ["wsgi.version"] = {1, 0},
        ["wsgi.multithread"] = false,
        ["wsgi.multiprocess"] = true,
        ["wsgi.run_once"] = true,
        ["wsgi.url_scheme"] = "http"
    }

    -- Required keys
    for _, key in ipairs(CGI_VARS) do
        environ[key] = all_envs[key] or ""
        all_envs[key] = nil
    end

    -- Optional keys with fallback
    for key, fallback in pairs(OPTIONAL_VARS) do
        environ[key] = all_envs[key] or fallback
        all_envs[key] = nil
    end

    -- HTTP headers
    for k, v in pairs(all_envs) do
        if stringx.startswith(k, "HTTP_") then
            environ[k] = v
        end
    end

    -- The start_response function records the desired status and headers
    local response_status = nil
    local response_headers = nil
    local function start_response (status, headers)
        if response_status ~= nil or response_headers ~= nil then
            error("start_response has been called")
        end
        response_status = status
        response_headers = HttpHeaders(headers)
    end

    local res = self.entry_function(environ, start_response)
    if response_status == nil then
        self:respond_with_500("The status code was unspecified")
        return
    end
    self:really_start_response(response_status, response_headers, res)
end

function base:respond_with_500 (html)
    local status_string = "500 Internal Server Error"
    self:really_start_response(status_string, nil, html or status_string)
end

function base:really_start_response (status, headers, res)
    if string.find(status, '^%d%d%d [%w ]+$') == nil then
        error('Bad status string')
    end
    if headers == nil then
        headers = HttpHeaders()
    end
    if headers:get("content-type") == nil then
        headers:set("content-type", "text/html; charset=utf-8")
    end

    -- Delete some headers
    headers:set("content-length", nil)
    headers:set("transfer-encoding", nil)
    headers:set("trailer", nil)

    local stdout = self.stdout

    -- Status code line
    stdout:write("HTTP/1.1 ")
    stdout:write(status)
    stdout:write("\r\n")
    -- All header lines
    for k, vlist in pairs(headers:get_data()) do
        for _, v in ipairs(vlist) do
            stdout:write(k)
            stdout:write(": ")
            stdout:write(v)
            stdout:write("\r\n")
        end
    end

    if type(res) == "string" then
        stdout:write("content-length: ")
        stdout:write(string.format("%d\r\n\r\n", string.len(res)))
        -- End of HTTP header
        stdout:write(res)
    elseif type(res) == "table" then  -- chunked encoding
        stdout:write("transfer-encoding: chunked\r\n\r\n")
        -- End of HTTP header
        if getmetatable(res) == nil then  -- array of strings
            self:write_chunked_lazily(res)
        else
            self:write_chunked_eagerly(res)
        end
    end
    stdout:flush()
end

function base:write_chunked_eagerly (iterable)
    local stdout = self.stdout

    for _, v in ipairs(iterable) do
        local strlen = string.len(v)
        if strlen > 0 then
            stdout:write(string.format("%X\r\n", strlen))
            stdout:write(v)
            stdout:write("\r\n")
            stdout:flush()
        end
    end
    stdout:write("0\r\n\r\n")
end

function base:write_chunked_lazily (arr)
    local stdout = self.stdout
    local max_index = #arr
    local start_index = 1

    while start_index <= max_index do
        local strlen = 0
        local current_index = start_index
        while strlen < 4000 and current_index <= max_index do
            strlen = strlen + string.len(arr[current_index])
            current_index = current_index + 1
        end
        -- Flush
        if strlen > 0 then
            stdout:write(string.format("%X\r\n", strlen))
            for i = start_index, current_index-1 do
                stdout:write(arr[i])
            end
            stdout:write("\r\n")
        end
        -- Update loop variables
        start_index = current_index
    end

    -- Write ending sequence
    stdout:write("0\r\n\r\n")
end

return wsgi
