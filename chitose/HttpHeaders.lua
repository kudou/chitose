--- -*- mode: Lua; lua-indent-level: 4; -*- ---

local class = require "pl.class"
local OrderedMap = require "pl.OrderedMap"

local Headers = class()
local g = {}

function Headers:_init (initial)
    self.ordered = OrderedMap()
    if initial == nil then
        return
    end
    for raw_k, raw_v in pairs(initial) do
        self:append(raw_k, raw_v)
    end
end

--- Indexing ---

function Headers:get_list (raw_k)
    local vlist = self.ordered:get(g.normalize_header(raw_k))
    if vlist == nil then
        return nil
    elseif #vlist == 0 then
        return nil
    else
        return vlist
    end
end

function Headers:get (raw_k)
    local vlist = self.ordered:get(g.normalize_header(raw_k))
    if vlist == nil then
        return nil
    elseif #vlist == 0 then
        return nil
    else
        return vlist[1]
    end
end

function g.normalize_header (raw_k)
    if string.find(raw_k, "^[%w%-]+$") == nil then
        error("Bad header format")
    end
    return string.lower(raw_k)
end

--- Setting ---

function Headers:set (raw_k, raw_v)
    local k = g.normalize_header(raw_k)
    if raw_v == nil and self.ordered:get(k) == nil then
        return
    end
    local vlist = nil
    if raw_v ~= nil then
        vlist = {}
        g.insert_into_header_vlist(vlist, raw_v)
    end
    self.ordered:set(k, vlist)
end

function Headers:append (raw_k, raw_v)
    local k = g.normalize_header(raw_k)
    if raw_v == nil then
        return
    end
    local vlist = self.ordered:get(k)
    if vlist == nil then
        vlist = {}
    end

    if type(raw_v) == "table" then
        for i, header_value in ipairs(raw_v) do
            g.insert_into_header_vlist(vlist, header_value)
        end
    else
        g.insert_into_header_vlist(vlist, raw_v)
    end
    self.ordered:set(k, vlist)
end

function g.sanitize_header_value (raw_v)
    if string.find(raw_v, "\n") ~= nil then
        error("Bad header value")
    end
    return raw_v
end

function g.insert_into_header_vlist (vlist, raw_v)
    local t = type(raw_v)
    if t == "string" then
        table.insert(vlist, g.sanitize_header_value(raw_v))
    elseif t == "number" or t == "boolean" then
        table.insert(vlist, tostring(raw_v))
    else
        -- Bad value type
        return
    end
end

--- Iterating ---

function Headers:get_data ()
    return self.ordered
end

return Headers
