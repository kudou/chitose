**Public Domain Statement:**

To the extent possible under law, I have waived all copyright and related or neighboring rights to the Chitose web development framework.

**Credits:**

- [Lua](https://www.lua.org/license.html) Copyright © 1994–2021 Lua.org, PUC-Rio. MIT license
- [PEP 3333](https://www.python.org/dev/peps/pep-3333/#copyright) by PJE et al. Public domain
