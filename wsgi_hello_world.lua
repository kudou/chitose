--- -*- mode: Lua; lua-indent-level: 4; -*- ---
local wsgi = require "chitose.wsgi"

local function entry_point (environ, start_response)
    local sorted = {}
    for k, _ in pairs(environ) do
        table.insert(sorted, k)
    end
    table.sort(sorted)

    local res = {}
    table.insert(res, "<code><dl>")
    for _, key in ipairs(sorted) do
        table.insert(res, string.format("<dt><b>%s</b></dt><dd>%s</dd>",
                                        key, environ[key]))
    end
    table.insert(res, "</dl></code>")
    start_response("200 OK", nil)
    return res
end

wsgi.run_wsgi(entry_point, "cgi")
